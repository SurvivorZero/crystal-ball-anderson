package android.nguyena.crystalball;

import java.util.Random;

public class Predictions {

    private static Predictions predictions;
    private String[] answers;

    private Predictions() {
        answers = new String[] {
                "JUST DO IT",
                "ayy lmao",
                "I don't know. Maybe?",
                "same"
        };
    }

    public static Predictions get() {
        if (predictions == null){
            predictions = new Predictions();
        }
        return predictions;
    }

    public  String getPrediction(){
        int rnd = new Random().nextInt(answers.length);
        return answers[rnd];
    }

}